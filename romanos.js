

/**
 * @param {number} num
 * @return {string}
 */
var intToRoman = function (num) {
	let valido = (num >= 1 && num <= 3999) ? true : false;
	if (valido) {
		const I = 1
		const V = 5
		const X = 10
		const L = 50
		const C = 100
		const D = 500
		const M = 1000
		let strRomano = ""
		if (num >= M) {
			while (num >= M) {
				num -= M
				strRomano += "M"
			}
		}
		if (num >= D && num < M && num != 0) {
			if (num >= M - 100) {
				strRomano += "CM"
				num -= 900
			}
			while (num >= D) {
				num -= D
				strRomano += "D"
			}
		}
		if (num >= C && num < D && num != 0) {
			if (num >= D - 100) {
				strRomano += "CD"
				num -= 400
			}
			while (num >= C) {
				num -= C
				strRomano += "C"
			}
		}
		if (num >= L && num < C && num != 0) {
			if (num >= C - 10) {
				strRomano += "XC"
				num -= 90
			}
			while (num >= L) {
				num -= L
				strRomano += "L"
			}
		}
		if (num >= X && num < L && num != 0) {
			if (num >= L - 10) {
				strRomano += "XL"
				num -= 40
			}
			while (num >= X) {
				num -= X
				strRomano += "X"
			}
		}
		if (num >= V && num < X && num != 0) {
			if (num == X - 1) {
				strRomano += "IX"
				num -= 9
			}
			while (num >= V) {
				num -= V
				strRomano += "V"
			}
		}
		if (num >= I && num < V && num != 0) {
			if (num == V - 1) {
				strRomano += "IV"
				num -= 4
			}
			while (num > 0) {
				num -= I
				strRomano += "I"
			}
		}
		return strRomano
	} else {
		return "Numero no valido!";
	}
};
